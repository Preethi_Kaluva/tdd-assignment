package com.company;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Spy;


import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class SquareTest {


    @Mock
    Read file;
    Square s;
    Square s1;

    @Spy
    Read file1;

    @BeforeEach
    public void setup() throws Exception {
        System.out.println("**** program started for this method ****");
        //now we mock the text file from which we read the number
        file=mock(Read.class);
        s=new Square();
        s.setreadobject(file);
    }

    @AfterEach
    public void setup1() {
        System.out.println("**** successfully executed the method ****");
    }


@Test
public void changeonaccess() throws Exception {

        when(file.reader(anyString())).thenReturn(100);
        boolean answer=s.isPerfect();
        verify(file).reader(anyString());
    //System.out.println( verify(file).reader(anyString())); // return value of verify

}


    @Test
public void isperfectsquare() throws Exception {
        // the following 3 lines check the functionality and returns true is number is a perfect square
//boolean answer=square.isPerfect();
//        assertTrue(answer);
//        System.out.println("yes it is a perfect square");


        when(file.reader(anyString())).thenReturn(100);
        boolean answer=s.isPerfect();
        assertTrue(answer);
     // verify(file).reader(anyString());



    }

    @Test
    public void isinvalidperfectsquare() throws Exception {
        // the following 3 lines check the functionality and returns true is number is not a perfect square
//boolean answer=square.isPerfect();
//            assertFalse(answer);
//            System.out.println("no it is not a perfect square");

        s1=new Square();
        file1=spy(new Read());
        s1.setreadobject(file1);

//        when(file1.reader(anyString())).thenReturn(10);  -- this does not work for spy and throws exception
doReturn(150).when(file1).reader(anyString());
        boolean answer=square.isPerfect();
        assertFalse(answer);


    }
    
    
    // this is the test case that handles a non-numeric character in the text file. 
    @Test
    public void nonumericcharacter() throws Exception{
        try{
            Read readnumber=new Read();
            double s=readnumber.reader("test.txt");
            System.out.println(s);}
        catch(InputMismatchException e){
            System.out.println("error is "+ e);
        }
        

    }
}